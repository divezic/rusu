import pandas as pd
import numpy as np
import random as rd
import matplotlib.pyplot as plt
import keras.layers as ly
import keras.models as ml
import keras.optimizers as om

def english_aplhabet():
    #get string that represents english aplhabet, helper function
    return 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

def create_lookup():
        numbers = list(c for c in '0123456789')
        upper_letters = list(c for c in english_aplhabet())
        num_numbers = list(ord(c)-48 for c in numbers)
        num_upper = list(ord(c)-55 for c in upper_letters)
        lookup = {}

        for i in range(len(numbers)):
            lookup[numbers[i]] = num_numbers[i]

        for i in range(len(upper_letters)):
            lookup[upper_letters[i]] = num_upper[i]
        
        return lookup


def write(text, where="", n=1, gan=None):
    if(gan == None):
        print("No GAN model specified")
        return

    generator = gan.generator

    lookup = create_lookup()

    text = text.upper()

    lines = text.split(' ')

    for img_idx in range(n):

        lines_img = []

        for line in lines:
            ln = [lookup[c] for c in line]
            labels = np.asarray(ln)
            n = len(labels)
            img, _ = gan.starting_noise(n)
            img = generator.predict([img, labels])

            #concatenate letters side by side
            img = np.concatenate(tuple(img), axis=1)
            lines_img.append(img)

        lines_l = [l.shape[1] for l in lines_img]
        max_l = max(lines_l)

        for i in range(len(lines_l)):
            l = lines_img[i].shape[1]
            if(l < max_l):
                zeros = np.zeros((28, max_l-l, 1))
                #add padding so all lines are of equal length
                lines_img[i] = np.concatenate((lines_img[i], zeros), axis=1)
                
        #concatenate vertically lines

        img = np.concatenate(tuple(lines_img), axis=0)

        img = np.absolute(img)
        img = 1-img
        #images need to be in RBG so they can be saved
        img = np.repeat(img, 3, axis=2)

        path = "{}_{}.jpg".format(where, img_idx)
        plt.imsave(path, img)

class GAN():
    class Discriminator:
        def __init__(self, input_shape=(28,28,1), latent=100, classes=36):
            self.input_shape = input_shape
            self.latent = latent
            self.classes = classes
        
        def get_model(self):
            label = ly.Input(shape=(1,))
            k = ly.Embedding(self.classes, self.latent)(label)
            image = self.input_shape[0] * self.input_shape[1]
            k = ly.Dense(image)(k)
            k = ly.Reshape((self.input_shape[0], self.input_shape[1], 1))(k)

            input_image = ly.Input(shape=self.input_shape)
            k = ly.Concatenate()([input_image, k])

            k = ly.Conv2D(filters=128, kernel_size=(3,3), strides=(2,2), padding='same', input_shape=self.input_shape)(k)
            k = ly.LeakyReLU(alpha=0.2)(k)

            k = ly.Conv2D(filters=128, kernel_size=(3,3), strides=(2,2), padding='same')(k)
            k = ly.LeakyReLU(alpha=0.2)(k)

            k = ly.Flatten()(k)
            k = ly.Dropout(0.4)(k)
            k = ly.Dense(1, activation='sigmoid')(k)

            model = ml.Model([input_image, label], k)
            optimizer = om.Adam(learning_rate=0.0002, beta_1=0.5)
            model.compile(loss='binary_crossentropy', optimizer=optimizer)
            return model

    class Generator:
        def __init__(self, latent=100, classes=36):
            self.latent = latent
            self.classes = classes

        def get_model(self):
            label = ly.Input(shape=(1,))
            k = ly.Embedding(self.classes, self.latent)(label)
            image = 7 * 7
            k = ly.Dense(image)(k)
            k = ly.Reshape((7, 7, 1))(k)

            input_image = ly.Input(shape=(self.latent,))

            shape = (7, 7, 128)
            image = shape[0] * shape[1] * shape[2]
            
            j = ly.Dense(image)(input_image)
            j = ly.LeakyReLU(alpha=0.2)(j)
            j = ly.Reshape(shape)(j)

            k = ly.Concatenate()([j, k])

            k = ly.Conv2DTranspose(filters=128, kernel_size=(3,3), strides=(2,2), padding='same')(k)
            k = ly.LeakyReLU(alpha=0.2)(k)

            k = ly.Conv2DTranspose(filters=128, kernel_size=(3,3), strides=(2,2), padding='same')(k)
            k = ly.LeakyReLU(alpha=0.2)(k)

            k = ly.Conv2D(filters=1, kernel_size=(7,7), activation='tanh', padding='same')(k)

            model = ml.Model([input_image, label], k)
            return model

    def __init__(self, train_path, input_shape=(28,28,1), latent = 100, classes=36):
        self.input_shape = input_shape
        self.latent = latent
        self.classes = classes
        self.discriminator = self.Discriminator(self.input_shape, self.latent, self.classes).get_model()
        self.generator = self.Generator(self.latent, self.classes).get_model()
        self.train_path = train_path

        self.load_data()


    def load_data(self):
        #load data from csv files
        csv_train = pd.read_csv(self.train_path)

        #convert data to numpy arrays
        train_x = np.asarray(csv_train.iloc[:,1:]).reshape([-1, 28, 28, 1])
        train_y = np.asarray(csv_train.iloc[:,0]).reshape([-1, 1])

        #ignore small letters
        train_ignore_small = [0 if i > 35 else 1 for i in train_y]

        train_x = [train_x[i] for i in range(len(train_x)) if train_ignore_small[i] == 1]
        train_y = [train_y[i] for i in range(len(train_y)) if train_ignore_small[i] == 1]

        #flip and rotate images
        train_x = np.array(list(np.rot90(np.flipud(x), k=-1) for x in train_x))

        #normalise data
        self.train_x = train_x/255.0

        #flatten arrays [[0], [1], ...] -> [0, 1, ...]
        self.train_y = np.ravel(train_y)


    def get_model(self):
        #define gan model where discriminator is not trainable
        self.discriminator.trainable = False

        generator_starting_image, generator_label = self.generator.input
        generator_output = self.generator.output

        k = self.discriminator([generator_output, generator_label])

        model = ml.Model([generator_starting_image, generator_label], k)

        optimizer = om.Adam(learning_rate=0.0002, beta_1=0.5)
        model.compile(loss='binary_crossentropy', optimizer=optimizer)

        return model

    def starting_noise(self, n):
        #generate latent space samples
        x = np.random.randn(self.latent * n)
        x = x.reshape(n, self.latent)
        labels = np.random.randint(0, self.classes, n)
        return [x, labels]

    def train(self, n_epochs=50, batch_size=128):
        def real_samples():
            n = self.train_x.shape[0]
            indices = np.random.randint(0, n, self.half_batch)
            x = self.train_x[indices]
            labels = self.train_y[indices]
            y = np.ones((self.half_batch, 1))
            return list(zip(x, labels, y))

        def fake_samples():
            x, labels = self.starting_noise(self.half_batch)
            x = self.generator.predict([x, labels])
            y = np.zeros((self.half_batch, 1))
            return list(zip(x, labels, y))

        self.gan_model = self.get_model()
        self.half_batch = int(batch_size/2)

        per_epoch = int(self.train_x.shape[0] / batch_size)

        for i in range(n_epochs):
            for _ in range(per_epoch):
                real_samp = real_samples()
                fake_samp = fake_samples()
                
                #train discriminator on half batch of real and fake samples
                samples = real_samp + fake_samp
                rd.shuffle(samples)

                x, labels, y = list(zip(*samples))
                x = np.asarray(x)
                labels = np.asarray(labels)
                y = np.asarray(y)

                self.discriminator.train_on_batch([x, labels], y)

                sn = self.starting_noise(batch_size)
                #train gan with locked discriminator weights, it was trained above
                gan_y = np.ones((batch_size, 1))
                self.gan_model.train_on_batch(sn, gan_y)
            
            #save images during training for evaluation
            write(text=english_aplhabet(), where='train_images/epoch_{}'.format(i+1), gan=self)
        
            #save generator models to be cherry picked
            self.generator.save('models/model_{}.h5'.format(i+1))

#set needed paths for dataset and model, if model exists
train_path = 'emnist-balanced-train.csv'
model_path = 'model.h5'

n_images = 5 #we create 5 images so we can see differences that occured beacuse of randomness
n_epochs = 50
batch_size = 128

latent = 100 #dimension of latent space out of which generator generates images
classes = 36 #36 total classes numbers and letters
input_shape = (28,28,1)

text = 'RUSU projekt' #insert text here, text must not contain anything but spaces, letters and numbers

gan = GAN(train_path, input_shape, latent, classes)
#use gan.train if there is no model, or just load model into generator
#gan.train(n_epochs, batch_size)
gan.generator = ml.load_model('models/model_50.h5')

#write images to files
write(text=text, where='img', n=n_images, gan=gan)
